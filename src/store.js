import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        sTimetableList: []
    },
    mutations: {
        smMutateTimetableList: (state, payload) => {
            state.sTimetableList = payload
        }
    },
    actions: {
        saChangeTimetableList({commit}, payload) {
            commit('smMutateTimetableList', payload);
        }
    },
    getters: {
        sgGetTimetableList(state) {
            return state.sTimetableList;
        }
    }
});

export default store;