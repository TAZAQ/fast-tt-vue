import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from "@/components/Blocks/Main";
import Error404 from "@/components/Blocks/Error404";
import PageTimetable from "@/components/Blocks/PageTimetable";
import Donations from "@/components/Donations";

Vue.use(VueRouter);

const project_home_page = 'fast-tt-vue';

let router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: `/${project_home_page}`,
            name: 'home',
            component: Main
        },

        {
            path: `/${project_home_page}/donations`,
            name: 'donations',
            component: Donations
        },

        {
            path: `/${project_home_page}/timetable/:item/:slug`,
            name: 'timetable',
            component: PageTimetable
        },

        {
            path: `/${project_home_page}/:page`,
            name: 'all',
            component: Error404,
        },

        {
            path: `/${project_home_page}/*`,
            name: '404',
            component: Error404
        },

        {
            path: `*`,
            name: '404',
            component: Error404
        }
    ]
});




export default router;